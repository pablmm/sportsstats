<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamsTable extends Migration {

	public function up()
	{
		Schema::create('teams', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 45);
			$table->integer('sport_id')->unsigned();
			$table->integer('tournament_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('teams');
	}
}