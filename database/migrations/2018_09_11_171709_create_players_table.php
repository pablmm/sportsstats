<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlayersTable extends Migration {

	public function up()
	{
		Schema::create('players', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('team_id');
			$table->string('first_name', 45);
			$table->string('last_name', 45);
			$table->integer('status')->default('1');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('players');
	}
}
