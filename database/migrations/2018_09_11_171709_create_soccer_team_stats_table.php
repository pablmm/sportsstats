<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSoccerTeamStatsTable extends Migration {

	public function up()
	{
		Schema::create('soccer_team_stats', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('team_id')->unsigned();
			$table->integer('games_played');
			$table->integer('games_win');
			$table->integer('games_draw');
			$table->integer('games_loss');
			$table->integer('goal_for');
            $table->integer('goal_against');
			$table->string('goal_difference',3);
			$table->integer('points');
            $table->timestamps();

        });
	}

	public function down()
	{
		Schema::drop('soccer_team_stats');
	}
}
