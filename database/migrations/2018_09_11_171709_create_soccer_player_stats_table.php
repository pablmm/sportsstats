<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSoccerPlayerStatsTable extends Migration {

	public function up()
	{
		Schema::create('soccer_player_stats', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('player_id')->unsigned();
			$table->integer('apps');
			$table->integer('mins');
			$table->integer('goals');
			$table->integer('assists');
			$table->integer('yel');
			$table->integer('red');
			$table->double('shots_per_game');
			$table->double('pass_success_percentage');
			$table->double('aerialswon');
			$table->double('man_of_the_match');
			$table->double('rating');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('soccer_player_stats');
	}
}