<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeamplayersTable extends Migration {

	public function up()
	{
		Schema::create('teamplayers', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('team_id')->unsigned();
			$table->integer('player_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('teamplayers');
	}
}