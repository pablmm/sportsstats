<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('teams', function(Blueprint $table) {
			$table->foreign('sport_id')->references('id')->on('sports')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('teams', function(Blueprint $table) {
			$table->foreign('tournament_id')->references('id')->on('tournaments')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('teamplayers', function(Blueprint $table) {
			$table->foreign('team_id')->references('id')->on('teams')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('teamplayers', function(Blueprint $table) {
			$table->foreign('player_id')->references('id')->on('players')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('tournaments', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('soccer_player_stats', function(Blueprint $table) {
			$table->foreign('player_id')->references('id')->on('players')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('soccer_team_stats', function(Blueprint $table) {
			$table->foreign('team_id')->references('id')->on('teams')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('teams', function(Blueprint $table) {
			$table->dropForeign('teams_sport_id_foreign');
		});
		Schema::table('teams', function(Blueprint $table) {
			$table->dropForeign('teams_tournament_id_foreign');
		});
		Schema::table('teamplayers', function(Blueprint $table) {
			$table->dropForeign('teamplayers_team_id_foreign');
		});
		Schema::table('teamplayers', function(Blueprint $table) {
			$table->dropForeign('teamplayers_player_id_foreign');
		});
		Schema::table('tournaments', function(Blueprint $table) {
			$table->dropForeign('tournaments_country_id_foreign');
		});
		Schema::table('soccer_player_stats', function(Blueprint $table) {
			$table->dropForeign('soccer_player_stats_player_id_foreign');
		});
		Schema::table('soccer_team_stats', function(Blueprint $table) {
			$table->dropForeign('soccer_team_stats_team_id_foreign');
		});
	}
}