<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoccerPlayerStatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('soccer_player_stats')->insert(
            array(
                array('player_id'=>1,'apps'=>27,'mins'=>2378,'goals'=>9,'assists'=>9,'yel'=>5,'red'=>0,'shots_per_game'=>4.3,'pass_success_percentage'=>82.4,'aerialswon'=>0.2,'man_of_the_match'=>8,'rating'=>7.79),
                array('player_id'=>2,'apps'=>27,'mins'=>2285,'goals'=>28,'assists'=>3,'yel'=>1,'red'=>0,'shots_per_game'=>3,'pass_success_percentage'=>79.1,'aerialswon'=>1.4,'man_of_the_match'=>8,'rating'=>7.53),
                array('player_id'=>3,'apps'=>26,'mins'=>2325,'goals'=>13,'assists'=>8,'yel'=>2,'red'=>0,'shots_per_game'=>2.8,'pass_success_percentage'=>79.7,'aerialswon'=>0.2,'man_of_the_match'=>7,'rating'=>7.68),
                array('player_id'=>4,'apps'=>1,'mins'=>90,'goals'=>0,'assists'=>0,'yel'=>0,'red'=>0,'shots_per_game'=>0,'pass_success_percentage'=>29.4,'aerialswon'=>0,'man_of_the_match'=>0,'rating'=>7.66)
            ));
    }
}
