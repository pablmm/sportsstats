<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert(
            array(
                array('name'=>'New York Red Bulls','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Atlanta United','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'New York City FC','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Columbus Crew','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Philadelphia Union','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Montreal Impact','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'New England Rev.','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'DC United','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Toronto FC','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Orlando City','sport_id'=>1,'tournament_id'=>10),
                array('name'=>'Chicago Fire','sport_id'=>1,'tournament_id'=>10)

            ));
    }
}
