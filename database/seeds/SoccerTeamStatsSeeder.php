<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoccerTeamStatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('soccer_team_stats')->insert(
            array(
                array('team_id'=>1,'games_played'=>28,'games_win'=>17,'games_draw'=>4,'games_loss'=>7,'goal_for'=>50,'goal_against'=>29,'goal_difference'=>'+21','points'=>55),
                array('team_id'=>2,'games_played'=>27,'games_win'=>16,'games_draw'=>6,'games_loss'=>5,'goal_for'=>56,'goal_against'=>33,'goal_difference'=>'+23','points'=>54),
                array('team_id'=>3,'games_played'=>29,'games_win'=>14,'games_draw'=>7,'games_loss'=>8,'goal_for'=>51,'goal_against'=>38,'goal_difference'=>'+13','points'=>49),
                array('team_id'=>4,'games_played'=>27,'games_win'=>12,'games_draw'=>7,'games_loss'=>8,'goal_for'=>35,'goal_against'=>34,'goal_difference'=>'+1','points'=>43),
                array('team_id'=>5,'games_played'=>27,'games_win'=>12,'games_draw'=>4,'games_loss'=>11,'goal_for'=>39,'goal_against'=>41,'goal_difference'=>'-2','points'=>40),
                array('team_id'=>6,'games_played'=>28,'games_win'=>11,'games_draw'=>3,'games_loss'=>14,'goal_for'=>37,'goal_against'=>45,'goal_difference'=>'-8','points'=>36),
                array('team_id'=>7,'games_played'=>27,'games_win'=>8,'games_draw'=>9,'games_loss'=>10,'goal_for'=>40,'goal_against'=>42,'goal_difference'=>'-2','points'=>33),
                array('team_id'=>8,'games_played'=>26,'games_win'=>8,'games_draw'=>7,'games_loss'=>11,'goal_for'=>43,'goal_against'=>44,'goal_difference'=>'-1','points'=>31),
                array('team_id'=>9,'games_played'=>27,'games_win'=>7,'games_draw'=>6,'games_loss'=>14,'goal_for'=>45,'goal_against'=>52,'goal_difference'=>'-7','points'=>27),
                array('team_id'=>10,'games_played'=>27,'games_win'=>7,'games_draw'=>3,'games_loss'=>17,'goal_for'=>40,'goal_against'=>62,'goal_difference'=>'-22','points'=>24),
                array('team_id'=>11,'games_played'=>27,'games_win'=>6,'games_draw'=>6,'games_loss'=>15,'goal_for'=>37,'goal_against'=>52,'goal_difference'=>'-15','points'=>24)

            ));
    }
}
