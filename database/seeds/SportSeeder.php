<?php

use Illuminate\Database\Seeder;

class SportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sports')->insert(
            array(
                array('name'=>'Soccer'),
                array('name'=>'Hockey'),
                array('name'=>'Football'),
                array('name'=>'BaseBall')

            ));
    }
}
