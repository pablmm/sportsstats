<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamPlayerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teamplayers')->insert(
            array(
                array('team_id'=>2,'player_id'=>1),
                array('team_id'=>2,'player_id'=>2),
                array('team_id'=>6,'player_id'=>3),
                array('team_id'=>7,'player_id'=>4)
            ));
    }
}
