<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            array(
                array('name'=>'pablo','email'=>'pablom@fwscience.com','level_id'=>1,'status_id'=>1,'first_name'=>'Pablo','last_name'=>'Morales','email_verified_at'=>null,'password'=>bcrypt('laravel'))
            ));
    }
}
