<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->truncateTables([
            'users',
            'password_resets',
            'countries',
            'players',
            'soccer_player_stats',
            'soccer_team_stats',
            'sports',
            'teamplayers',
            'tournaments',
        ]);

        $this->call(CountrySeeder::class);
        $this->call(SportSeeder::class);
        $this->call(TournamentSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(PlayersSeeder::class);
        $this->call(TeamPlayerSeeder::class);
        $this->call(SoccerTeamStatsSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SoccerPlayerStatsSeeder::class);
    }

    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
