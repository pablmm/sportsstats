<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->insert(
            array(
                array('team_id'=>2,'first_name'=>'Miguel','last_name'=>'Almiron','status'=>1),
                array('team_id'=>2,'first_name'=>'Josef','last_name'=>'Martinez','status'=>1),
                array('team_id'=>6,'first_name'=>'Ignacio','last_name'=>'Piatti','status'=>1),
                array('team_id'=>7,'first_name'=>'Brad','last_name'=>'Knighton','status'=>1)
            ));
    }
}
