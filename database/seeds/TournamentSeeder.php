<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tournaments')->insert(
            array(
                array('name'=>'Premier League','country_id'=>186),
                array('name'=>'Serie A','country_id'=>82),
                array('name'=>'La Liga','country_id'=>164),
                array('name'=>'Bundesliga','country_id'=>64),
                array('name'=>'Ligue 1','country_id'=>60),
                array('name'=>'Liga NOS','country_id'=>139),
                array('name'=>'Eredivisie','country_id'=>122),
                array('name'=>'Premier League','country_id'=>143),
                array('name'=>'Brasileirão','country_id'=>24),
                array('name'=>'Major League Soccer','country_id'=>187),
                array('name'=>'Super Lig','country_id'=>180),
                array('name'=>'Championship','country_id'=>186),
                array('name'=>'Primera División','country_id'=>7),
                array('name'=>'Super league','country_id'=>36),
                array('name'=>'Bundesliga II','country_id'=>64),
                array('name'=>'UEFA Champions League','country_id'=>197),
                array('name'=>'UEFA Europa League','country_id'=>197),
                array('name'=>'FIFA World Cup','country_id'=>198)

            ));
    }
}
