<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\users;
use App\Http\Resources\user as userResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('users');
    }

    public function usersAx()
    {
        return new userResource(users::all());
    }
}
