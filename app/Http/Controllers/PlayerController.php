<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Player;
use App\Http\Resources\player as PlayersResource;

class PlayerController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      //return new PlayersResource(player::all());
      return view('players');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
//      return new PlayersResource($request);

      $player = new Player; //Player::create($request->all());

      $player->first_name = $request->input('first_name');
      $player->last_name = $request->input('last_name');
      $player->team_id = $request->input('team_id');

      $player->save();

      return $player;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request,$id)
  {
      $player->update($request->all());
      return new PlayersResource($player);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

    public function playersAx()
    {
        return new PlayersResource(player::with('team_player')->get());
    }


}

?>
