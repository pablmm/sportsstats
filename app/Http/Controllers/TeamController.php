<?php 

namespace App\Http\Controllers;

use App\Model\Team;
use Illuminate\Http\Request;
use App\Http\Resources\Team as TeamResource;

class TeamController extends Controller 
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      //return new TeamResource(Team::all());
      return view('teams');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      //return Team::create($request->all());
      return redirect()->route('teams.index');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
      $team = new Team;

      $team->name = $request->input('name');
      $team->sport_id = $request->input('sport_id');
      $team->tournament_id = $request->input('tournament_id');

      $team->save();

      return $team;
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
      $teams->update($request->all());
      return new TeamResource($teams);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    
  }

    public function teamsAx()
    {

        return new TeamResource(Team::all()->load('team_sport','team_tournament'));
    }
  
}

?>
