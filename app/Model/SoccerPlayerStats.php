<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SoccerPlayerStats extends Model 
{

    protected $table = 'soccer_player_stats';
    public $timestamps = true;


    public function player()
    {
        return $this->hasOne('App\Model\Player', 'id','player_id');
    }

}
