<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TeamPlayer extends Model 
{

    protected $table = 'teamplayers';
    public $timestamps = true;

    public function team_player()
    {
        return $this->hasMany('Team', 'team_id');
    }

    public function player_team()
    {
        return $this->hasOne('Player', 'player_id');
    }

}
