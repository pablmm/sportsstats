<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SoccerTeamStats extends Model 
{

    protected $table = 'soccer_team_stats';
    public $timestamps = true;

    public function team()
    {
        return $this->hasOne('App\Model\Team', 'id','team_id');
    }

}
