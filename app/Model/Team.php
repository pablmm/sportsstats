<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';
    public $timestamps = true;

    public function team_sport()
    {
        return $this->hasOne('App\Model\Sports', 'id','sport_id');
    }

    public function team_tournament()
    {
        return $this->hasOne('App\Model\Tournament', 'id','tournament_id');
    }

    public function team_stats()
    {
        return $this->hasOne('App\Model\SoccerTeamStats', 'id','team_id');
    }

    public function find($id){

    }


}
