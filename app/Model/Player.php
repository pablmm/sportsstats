<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 9/11/18
 * Time: 3:55 PM
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{

    protected $table = 'players';
    public $timestamps = true;

    protected $fillable = ['first_name', 'last_name', 'team_id'];

    public function player_stats()
    {
        return $this->hasOne('App\Model\SoccerPlayerStats', 'id','player_id');
    }

    public function team_player()
    {
        return $this->hasOne('App\Model\Team', 'id','team_id');
    }

}
