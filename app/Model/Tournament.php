<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model 
{

    protected $table = 'tournaments';
    public $timestamps = true;

    public function tournament_country()
    {
        return $this->hasOne('App\Model\Country', 'id','country_id');
    }

}
