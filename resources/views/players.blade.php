@extends('welcome')
@section('content')
    <div id="players">

        <div class="row">
            <div class="col-md-3">
                <label for="">First Name</label>
                <input type="text" class="form-control" v-model="newPlayer.first_name">
            </div>
            <div class="col-md-3">
                <label for="">Last Name</label>
                <input type="text" class="form-control" v-model="newPlayer.last_name">
            </div>
            <div class="col-md-3">
                <label for="">Team</label>
                <select name="" id="" class="form-control" v-model="newPlayer.team_id" placeholder="Select a team">
                    <option value="">Select Team</option>
                    <option v-for="(t, i) in team" :key="i" :value="t.id">@{{ t.name }}</option>
                </select>
                <input type="hidden" class="form-control">
            </div>
            <div class="col-md-3 text-left">
                <button class="btn btn-primary" @click="addPlayer">@{{ buttonMessage  }}</button>
                <button class="btn btn-default" @click="resetForm">Reset</button>
            </div>
        </div>
        <hr />
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Team</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in player">
                <td>@{{ t.id }}</td>
                <td>@{{ t.first_name }} @{{ t.last_name }}</td>
                <td>@{{ t.team_player.name }}</td>
                <td>
                    <button class="btn btn-primary" @click="editFormPlayer(t)">Edit</button>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js"></script>
    <script>
        new Vue({
            el:'#players',
            data:{
                player:[],
                newPlayer: {},
                team: [],
                buttonMessage: "+ Add Player",
            },
            mounted:function(){
                this.getData();
                this.getTeams();
            },
            methods:{
                getData:function() {
                    var that = this
                    axios.get('{{route('playersAx')}}').then(function(response){
                        that.player = response.data.data;
                    })
                },
                getTeams: function() {
                    var that = this;
                    axios.get('{{route('teamsAx')}}').then(function(response){
                        that.team = response.data.data;
                    })
                },
                resetForm() {
                    this.buttonMessage = "+ Add Player";
                    this.newPlayer.first_name = null;
                    this.newPlayer.last_name = null;
                    this.newPlayer.team_id = null;
                },
                addPlayer: function() {
                    var that = this;
                    axios.post('{{ url('player') }}', this.newPlayer).then(function (response) {

                    });
                    this.getData();
                },
                editFormPlayer(id) {
                    this.buttonMessage = "Edit Player";
                    this.newPlayer = id;
                }
            }
        })
    </script>
    <script>
        new Vue({
            el:'#app',
            data:{
                message: "PLAYERS"
            }
        })
    </script>
@endsection
