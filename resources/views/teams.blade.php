@extends('welcome')
@section('content')
    <div id="teams">

        <div class="row">
            <div class="col-md-3">
                <label for="">Team Name</label>
                <input type="text" class="form-control" v-model="newTeam.name">
            </div>
            <div class="col-md-3">
                <label for="">Sport</label>
                <select name="" id="" class="form-control" v-model="newTeam.sport_id">
                    <option value="">Select a Sport</option>
                    <option v-for="(s, i) in sports" :key="i" :value="s.id" >@{{ s.name }}</option>
                </select>
            </div>
            <div class="col-md-3">
                <label for="">Tournament</label>
                <select name="" id=""class="form-control" v-model="newTeam.tournament_id">
                    <option value="">Select a Tournament</option>
                    <option v-for="(t, i) in tournaments" :key="i" :value="t.id" >@{{ t.name }}</option>
                </select>
            </div>
            <div class="col-md-3">
                <button class="btn btn-success" @click="addTeam">
                    Add Team
                </button>
            </div>
        </div>

        <hr>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Sport</th>
                <th scope="col">Tournament</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in team">
                <td>@{{ t.id }}</td>
                <td>@{{ t.name }}</td>
                <td>@{{ t.team_sport.name }}</td>
                <td>@{{ t.team_tournament.name }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el:'#teams',
            data:{
                team:[],
                newTeam:{},
                sports: [],
                tournaments: [],
            },
            mounted:function(){
                this.getData();
                this.getTournaments();
                this.getSports();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('teamsAx')}}').then(function(response){
                        that.team = response.data.data;
                        console.log(response.data);
                    })
                },
                getSports() {
                    var that = this;
                    axios.get('{{route('sportsAx')}}').then(function(response){
                        that.sports = response.data.data;
                    }).catch(function (error) {
                        console.log(error)
                    });
                },
                getTournaments() {
                    var that = this;
                    axios.get('{{route('tournamentAx')}}').then(function(response){
                        that.tournaments = response.data.data;
                    });
                },
                resetForm() {
                    this.newTeam.name = null;
                    this.newTeam.sport_id = null;
                    this.newTeam.tournament_id = null;
                },
                addTeam() {
                    var that = this;
                    axios.post('{{ url('team') }}', this.newTeam).then(function(response) {
                        that.resetForm();
                    });

                    this.getData();
                },
            }
        })
    </script>
    <script>
        new Vue({
            el:'#app',
            data:{
                message: "TEAMS"
            }
        })
    </script>
@endsection
