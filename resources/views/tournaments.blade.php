@extends('welcome')
@section('wel')
    <div id="app">

    </div>
@endsection
@section('content')
    <div id="tournaments">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Country</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in tournament">
                <td>@{{ t.id }}</td>
                <td>@{{ t.name }}</td>
                <td>@{{ t.tournament_country.name }}</td>
            </tr>
            </tbody>
        </table>

    </div>
    @endsection

@section('js')
    <script>
        new Vue({
            el:'#tournaments',
            data:{
              tournament:[]
            },
            mounted:function(){
                this.getData();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('tournamentAx')}}').then(function(response){
                    that.tournament = response.data.data;
                    console.log(response.data);
                    })
                }
            }
        })
    </script>
@endsection
