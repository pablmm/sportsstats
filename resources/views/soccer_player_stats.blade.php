@extends('welcome')
@section('content')
    <div id="sps">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Player</th>
                <th scope="col">apps</th>
                <th scope="col">mins</th>
                <th scope="col">goals</th>
                <th scope="col">assists</th>
                <th scope="col">yel</th>
                <th scope="col">red</th>
                <th scope="col">SPG</th>
                <th scope="col">PSP%</th>
                <th scope="col">Aerialswon</th>
                <th scope="col">MOM</th>
                <th scope="col">Rating</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in SoccerPlayerStats">
                <td>@{{ t.id }}</td>
                <td>@{{ t.player.first_name }} @{{ t.player.last_name }}</td>
                <td>@{{ t.apps }}</td>
                <td>@{{ t.mins }}</td>
                <td>@{{ t.goals }}</td>
                <td>@{{ t.assists }}</td>
                <td>@{{ t.yel }}</td>
                <td>@{{ t.red }}</td>
                <td>@{{ t.shots_per_game }}</td>
                <td>@{{ t.pass_success_percentage }}</td>
                <td>@{{ t.aerialswon }}</td>
                <td>@{{ t.man_of_the_match }}</td>
                <td>@{{ t.rating }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el:'#sps',
            data:{
                SoccerPlayerStats:[]
            },
            mounted:function(){
                this.getData();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('SPSAx')}}').then(function(response){
                        that.SoccerPlayerStats = response.data.data;
                        console.log(that.SoccerPlayerStats);
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
            }
        })
    </script>
@endsection
