@extends('welcome')
@section('content')
    <div id="us">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Email</th>
                <th scope="col">Level</th>
                <th scope="col">Status</th>
                <th scope="col">Name</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in user">
                <td>@{{ t.id }}</td>
                <td>@{{ t.name }}</td>
                <td>@{{ t.email }}</td>
                <td>@{{ t.level_id }}</td>
                <td>@{{ t.status_id }}</td>
                <td>@{{ t.first_name }} @{{ t.last_name }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el:'#us',
            data:{
                user:[]
            },
            mounted:function(){
                this.getData();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('usersAx')}}').then(function(response){
                        that.user = response.data.data;
                        console.log(response.data);
                    })
                }
            }
        })
    </script>
    <script>
        new Vue({
            el:'#app',
            data:{
                message: "USERS"
            }
        })
    </script>
@endsection
