@extends('welcome')
@section('content')
    <div id="sts">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Team</th>
                <th scope="col">Games Played</th>
                <th scope="col">Win</th>
                <th scope="col">Draw</th>
                <th scope="col">Loss</th>
                <th scope="col">Goals</th>
                <th scope="col">Goals Agains</th>
                <th scope="col">Goals Difference</th>
                <th scope="col">Points</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in SoccerTeamStats">
                <td>@{{ t.id }}</td>
                <td>@{{ t.team.name }}</td>
                <td>@{{ t.games_palyed }}</td>
                <td>@{{ t.games_win }}</td>
                <td>@{{ t.games_draw }}</td>
                <td>@{{ t.games_loss }}</td>
                <td>@{{ t.goal_for }}</td>
                <td>@{{ t.goal_against }}</td>
                <td>@{{ t.goal_difference }}</td>
                <td>@{{ t.points }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el:'#sts',
            data:{
                SoccerTeamStats:[]
            },
            mounted:function(){
                this.getData();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('STSAx')}}').then(function(response){
                        that.SoccerTeamStats = response.data.data;
                        console.log(that.SoccerTeamStats);
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
            }
        })
    </script>
@endsection
