@extends('welcome')
@section('content')
    <div id="sport">

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="t in Sport">
                <td>@{{ t.id }}</td>
                <td>@{{ t.name }}</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection

@section('js')
    <script>
        new Vue({
            el:'#sport',
            data:{
                Sport:[]
            },
            mounted:function(){
                this.getData();
            },
            methods:{
                getData:function(){
                    var that = this
                    axios.get('{{route('sportsAx')}}').then(function(response){
                        that.Sport = response.data.data;
                        console.log(that.Sport);
                    }).catch(function (error) {
                        console.log(error)
                    });
                }
            }
        })
    </script>
    <script>
        new Vue({
            el:'#app',
            data:{
                message: "SPORTS"
            }
        })
    </script>
@endsection
