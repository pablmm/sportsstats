<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('details', 'API\UserController@details');
});


Route::get('tournament', 'TournamentController@tournamentAx')->name('tournamentAx');
Route::get('teams', 'TeamController@teamsAx')->name('teamsAx');
Route::get('players', 'PlayerController@playersAx')->name('playersAx');
Route::get('soccerplayerstats', 'SoccerPlayerStatsController@SPSAx')->name('SPSAx');
Route::get('soccerteamstats', 'SoccerTeamStatsController@STSAx')->name('STSAx');
Route::get('users', 'UserController@usersAx')->name('usersAx');
Route::get('sport', 'SportController@sportsAx')->name('sportsAx');
