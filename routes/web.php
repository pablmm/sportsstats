<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('team', 'TeamController');
Route::resource('player', 'PlayerController');
Route::resource('teamplayer', 'TeamPlayerController');
Route::resource('sport', 'SportController');
Route::resource('tournament', 'TournamentController');
Route::resource('country', 'CountryController');
Route::resource('soccerplayerstats', 'SoccerPlayerStatsController');
Route::resource('soccerteamstats', 'SoccerTeamStatsController');
Route::resource('users', 'UserController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
